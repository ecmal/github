import {Client as HttpClient} from 'http/client';

export class Client extends HttpClient {
    constructor(username,password){
        super('https://api.github.com',{
            'Authorization' : `Basic ${new Buffer(`${username}:${password}`).toString('base64')}`,
            'User-Agent' : 'Awesome-Octocat-App'
        });
    }
    get(path,query?){
        return this.request({method:'GET',path:path,query:query}).then((r:any)=>JSON.parse(r.content.toString()))
    }
    user(name?){
        return this.get('/user');
    }
    userOrgs(){
        return this.get('/user/orgs');
    }
    users(name){
        return this.get(`/users/${name}`);
    }
    repos(name){
        return this.get(`/repos/${name}`);
    }
    orgs(name){
        return this.get(`/orgs/${name}`);
    }
    searchRepo(name){
        return this.get(`/search/repositories`,{q:`${name} in:name`})
    }
}